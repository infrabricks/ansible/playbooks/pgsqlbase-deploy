# PG base deploy

Ansible playbook to deploy a postgreSQL base instance and create a DB / user.

## Requirements

* You need roles bellow to use this playbook :
  * [PostgreSQL install](https://gitlab.mim-libre.fr/infrabricks/ansible/roles/pgsql-install)
  * [PGSQL ops](https://gitlab.mim-libre.fr/infrabricks/ansible/roles/pgsql-ops)
* Ansible >= 4

## OS

* Debian

## Playbook Example

An example of playbook

```
- name: Deploy pgsql base
  hosts: all
  vars_files:
    - vars/main.yml
    - vars/vault.yml
  tasks:
    - name: Include PostgreSQL install role
      ansible.builtin.include_role:
        name: pgsql_install
    - name: Include PGSQL ops role
      ansible.builtin.include_role:
        name: pgsql_ops
```

## Author Information

* [Stéphane Paillet](mailto:spaillet@ethicsys.fr)
